export function nullIfUndefined(obj, comp) {
    return obj ? comp : null;
}

export function nullIfDefined(obj, comp) {
    return obj ? null : comp;
}

export function nullIfEmpty(arr, comp) {
    return arr && arr.length == 0 ? null : comp;
}

export function getCurrentPosition(resolve, reject) {
    navigator.geolocation.getCurrentPosition(
        (position) => {
            resolve(position.coords.latitude, position.coords.longitude);
        },
        (error) => reject(error),
        {enableHighAccuracy: false, timeout: 5000}
    );
}

export function logError(error) {
    console.log(JSON.stringify(error));  // TODO log error
}