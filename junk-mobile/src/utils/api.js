import Config from "react-native-config";

class Api {
    static headers() {
        return {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "dataType": "json"
        }
    }

    static get(route) {
        return this.xhr(route, null, "GET");
    }

    static put(route, params) {
        return this.xhr(route, params, "PUT")
    }

    static post(route, params) {
        return this.xhr(route, params, "POST")
    }

    static remove(route, params) {
        return this.xhr(route, params, "DELETE")
    }

    static xhr(route, params, verb) {
        const host = Config.API_URL;
        const _cache = Config.ENV === 'dev' ? `_c=${Math.random()}`: '';
        const _cacheParam = route.indexOf("?") > 0 ? "&" + _cache : "?" + _cache;
        const url = `${host}${route}${_cacheParam}`;

        let options = Object.assign({ method: verb }, params ? { body: JSON.stringify(params) } : null );
        options.headers = Api.headers();
        return fetch(url, options).then( resp => {
            let json = resp.json();
            if (resp.ok) {
                return json
            }
            return json.then(err => {throw err});
        });
    }
}

export default Api;
