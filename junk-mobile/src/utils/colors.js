export default {
    black: "#444444",
    white: "#FFFFFF",
    blue: "#3456DA",
    red: "#F91515",
    gray: "#AAAAAA",
    pink: "#EFBDEB",
    mauve: "#B68CB8",
};