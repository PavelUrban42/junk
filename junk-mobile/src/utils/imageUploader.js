import Config from "react-native-config";
import FileUpload from "NativeModules";

const fu = FileUpload.FileUpload;

class ImageUploader {

    static uploadImage(filename, filepath) {
        return new Promise(function (resolve, reject) {
            const host = Config.API_URL;
            const route = `/images`;
            const url = `${host}${route}`;

            var obj = {
                uploadUrl: url,
                method: 'POST', // default 'POST',support 'POST' and 'PUT'
                headers: {
                    'Accept': 'application/json'
                },
                fields: {},
                files: [{
                    filename: filename, // require, file name
                    filepath: filepath, // require, file absoluete path
                }]
            };
            fu.upload(obj, function (err, result) {
                if (result && result.data) {
                    var data = JSON.parse(result.data);
                    if (data.url) {
                        resolve(data.url);
                    } else {
                        reject('Image wasn\'t uploaded, please try again');
                    }
                } else {
                    reject(err);
                }
            });
        });

    }
}

export default ImageUploader;