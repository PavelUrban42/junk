export function isNotEmpty(value) {
    return value != undefined && value.trim() != '';
}

export function isUndefined(value) {
    return value === undefined;
}

export function isInitializedAndNotEmpty(value) {
    return isUndefined(value) || isNotEmpty(value);
}

export function validate(rules, obj, field) {
    let objRules = rules[field];
    if (!rules) {
        return null;
    }
    let value = obj[field];
    return objRules.map(rule => {
            if (!rule.func.call(this, value)) return rule.errorMessage;
        }).filter(message => message != undefined).join(",") || null;

}