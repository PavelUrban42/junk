import Config from "react-native-config";
const API_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json";

export default class PlacesGeocoder {

    static getPlaces (lat, lng) {
        const api_key = Config.GOOGLE_API_KEY;
        const url = `${API_URL}?radius=100&types=point_of_interest|establishment&key=${api_key}&location=${lat},${lng}`;
        return fetch(url).then( resp => {
            return resp.json().then(json => {
                if (json.status === "OK") {
                    return json.results.map((result) => {
                        return {
                            name: result.name,
                            location: result.geometry.location,
                            formattedAddress: result.vicinity
                        };
                    });
                }
            })
        }).catch(err => {console.log(err); return null});
    }
}