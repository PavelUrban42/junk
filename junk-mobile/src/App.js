//noinspection JSUnresolvedVariable
import React, { Component } from "react";
import Root from "./containers/Root";
import { Provider } from "react-redux";
import configureStore from "./middleware/configureStore";

const store = configureStore({});

class App extends Component {
    constructor(props) {
        super(props);
    };

    render () {
        return (
            <Provider store={store}>
                <Root />
            </Provider>
        );
    }
}

export default App;