//noinspection JSUnresolvedVariable
import React, {Component, PropTypes} from "react";
//noinspection JSUnresolvedVariable
import {
    StyleSheet,
    Dimensions,
    View,
    Image,
    NativeModules
} from "react-native";
//noinspection JSUnresolvedVariable
import {GoogleSignin, GoogleSigninButton} from 'react-native-google-signin';
import {Container, Content, InputGroup, Input, Button, Icon} from 'native-base';
import {connect} from "react-redux";
import {ActionCreators} from "../../actions";
import {bindActionCreators} from "redux";
import {Actions as scenes} from "react-native-router-flux";
import Spinner from 'react-native-loading-spinner-overlay';
import DialogAndroid from "react-native-dialogs";
import DeviceInfo from "react-native-device-info";

const {RNGoogleSignin} = NativeModules;

class LoginScreen extends Component {
    static contextTypes = {
        routes: PropTypes.object.isRequired,
    };

    constructor(props) {
        super(props);

        GoogleSignin.hasPlayServices({autoResolve: true}).then(() => {
            GoogleSignin.configure({
                scopes: [
                    'email', 'profile'
                ],
                webClientId: "193791589708-n4fqvlkpmsqakqj4tgfe11vgsvl0jmsq.apps.googleusercontent.com",
                offlineAccess: true
            })
        }).catch((err) => {
            console.log("Play services error", err.code, err.message);
        });

        var deviceId = DeviceInfo.getUniqueID();
        this.state = {
            loading: true,
            nickname: '',
            deviceId: deviceId
        };
        this.props.getUserByDevice(deviceId).then(() => {
            if (this.props.user && this.props.user.token != "") {
                scenes.profile();
            } else {
                this.setState({loading: false});
            }
        });

    }

    getAccessToken(user) {
        return RNGoogleSignin.getAccessToken(user);
    }
    showDialog = (options) => {
        var dialog = new DialogAndroid();
        dialog.set(options);
        dialog.show();
    };

    showError = (message) => {
        var options = {
            title: 'Error',
            content: 'An error occurred: ' + message,
            positiveText: 'OK'
        };
        this.showDialog(options)
    };

    onPressSignIn = () => {
        GoogleSignin.signIn().then((user) => {
            this.getAccessToken(user).then((token) => {
                this.setState({loading: true});
                this.props.authorizeUser(token).then(()=>{
                    scenes.profile();
                });
            })
        }).catch((err) => {
            this.showError("Authorization was canceled");
        }).done();
    };

    loginDevice = () => {
        if (this.state.nickname.trim() != '') {
            var user = {
                deviceId : this.state.deviceId,
                name: this.state.nickname
            };
            this.props.authorizeUserViaDevice(user).then(()=>{
                if (this.props.user && this.props.user.token != "") {
                    scenes.profile();
                } else {
                    this.showError("Authorization failed. Please check internet connection and try again");
                }
            }).catch((err) => {
                this.showError("Authorization failed. Please check internet connection and try again");
            });
        }
    };

    updateNickname(text) {
        this.setState({nickname: text});
    }

    render() {
        console.log('LoginScreen render');
        return (
            <Container style={styles.container}>
                <Content style={styles.content}>
                    <Spinner visible={this.state.loading}
                             textStyle={{color: '#FFF'}}
                             overlayColor='rgba(190,190,190,1)'
                             textContent="Loading..."
                    />
                    <Image style={styles.userIcon} source={require('./default.png')}/>
                    <View style={styles.nameLoginContainer}>
                        <InputGroup style={styles.nameInput}>
                            <Input placeholder="Nickname" onChangeText={(text)=> { this.updateNickname(text)}}/>
                        </InputGroup>
                        <Button iconRight onPress={this.loginDevice} title={"Login"}  style={styles.nameButton}>
                            <Icon name='ios-arrow-forward' />
                        </Button>
                    </View>
                    <GoogleSigninButton
                        style={styles.button}
                        size={GoogleSigninButton.Size.Standard}
                        color={GoogleSigninButton.Color.Dark}
                        onPress={this.onPressSignIn}/>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    userIcon: {
        marginTop: 16,
        width: 268,
        height: 230
    },
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 0
    },
    content: {
        marginTop: 55
    },
    text: {
        alignSelf: 'center',
        color: '#fff',
        fontSize: 16,
        marginTop: 5
    },
    button: {
        marginTop: 20,
        width: 268,
        height: 48
    },
    nameLoginContainer: {
        flexDirection: 'row'
    },
    nameInput: {
        flex: 7
    },
    nameButton: {
        flex: 1
    }

});

function mapStateToProps(state) {
    return state;
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
