import React, {Component, PropTypes} from "react";
import {TextInput, View, TouchableOpacity, ScrollView} from "react-native";
import {Container, Content, List, ListItem, InputGroup, Input, Icon, Text, Button} from "native-base";
import JunkCamera from "../../components/JunkCamera";
import InputTag from "../../components/tags/InputTag";
import SelectedLocation from "../../components/article-form/SelectedLocation";
import ErrorMessage from "../../components/article-form/ErrorMessage";
import ArticlePlacePicker from "../../components/article-form/ArticlePlacePicker";
import CurrentLocations from "../../components/article-form/CurrentLocations";
import DialogAndroid from "react-native-dialogs";
import styles from "../../styles/articles/styles";
import {ActionCreators} from "../../actions/index";
import bindActionCreators from "redux/es/bindActionCreators";
import connect from "react-redux/src/components/connect";
import PlacesGeocoder from "../../utils/placesGeocoder";
import {Actions, ActionConst} from "react-native-router-flux";
import {isInitializedAndNotEmpty, validate} from "../../utils/validator";
import {nullIfUndefined, getCurrentPosition, logError} from "../../utils/defaults";

class CreateScreen extends Component {

    static propTypes = {
         user: PropTypes.object.isRequired
    };

    componentWillMount() {
        let newArticle = {
                author: this.props.user.profile.name,
                user: this.props.user.profile.id
        };
        var validationRules = {
            topic: [{
                func: isInitializedAndNotEmpty,
                errorMessage: 'Field is required'
            }],
            description: [{
                func: isInitializedAndNotEmpty,
                errorMessage: 'Field is required'
            }]
        };
        this.setState({newArticle: newArticle, addresses: [], validationRules: validationRules});
    }

    componentDidMount() {
        getCurrentPosition(this.updateAddresses, logError);
    }

    // ------- update state block ------
    updateAddresses = (latitude, longitude) => {
        PlacesGeocoder.getPlaces(latitude, longitude).then(res => {
            this.setState({
                addresses: res || []
            });
        }).catch(logError);
    };

    updateState = (obj) => {
        let newArticle = Object.assign(this.state.newArticle, obj);
        this.setState({newArticle: newArticle});
    };

    validateField = (field) => {
        return validate(this.state.validationRules, this.state.newArticle, field);
    };

    setDefaultValues = () => {
        let article = this.state.newArticle;
        Object.keys(this.state.validationRules).forEach(key => {
            if (article[key] === undefined) {
                article[key] = '';
            }
        });
        this.setState({article});
    };

    // ------- ROUTING ------
    returnToList = () => {
        Actions.articleList({type: ActionConst.RESET});
    };

    returnToUserArticles = () => {
        Actions.articleList({type: ActionConst.RESET});
    };

    // ----- COMPONENT ACTIONS -----

    pickLocation = (address) => {
        this.updateState({location: address});
    };

    onImageSelected = (url) =>  {
        let images = this.state.newArticle.images || [];
        this.updateState({images: [...images, url]});
    };

    focusTo = (field) =>  {
        if (this.refs[field]) {
            this.refs[field].focus(); // RN input
        } else if (this[field]) {
            this[field]._textInput.focus(); // nativebase input
        }
    };

    submit = () => {
        this.setDefaultValues();
        var invalidKeys = Object.keys(this.state.validationRules).map(key => {
            if (this.validateField(key)) {
                return key;
            }
        }).filter(key => key != undefined);
        if (invalidKeys.length > 0) {
            var fieldname = "_" + invalidKeys[0] + "Input";
            this.focusTo(fieldname); // focus to first invalid field
            return;
        }
        let newArticle = this.state.newArticle;
        if (this.state.newArticle.location) {
            let location = {
                formattedAddress: newArticle.location.formattedAddress,
                geo: {
                    latitude: newArticle.location.location.lat,
                    longitude: newArticle.location.location.lng
                },
                name: newArticle.location.name
            };

            newArticle = Object.assign(this.state.newArticle, location);
        }
        this.props.addArticle(newArticle)
            .then(this.showSuccess)
            .catch(this.showError);
    };

    render() {
        return <Container style={styles.createContainer}>
            <Content>
                <JunkCamera onImageSelected={imageUrl => this.onImageSelected(imageUrl)}/>
                <List>
                    <ListItem style={styles.input}>
                        <View style={styles.validatedInput} refreshing>
                            <InputGroup error={this.validateField('topic')}>
                                <Input placeholder="Topic*" onChangeText={(text)=> { this.updateState({topic: text})}} ref={c => this._topicInput = c}/>
                            </InputGroup>
                            <ErrorMessage text={this.validateField('topic')}/>
                        </View>
                    </ListItem>
                    <ListItem style={styles.input}>
                        <View style={styles.validatedInput} refreshing>
                            <TextInput multiline numberOfLines={3} placeholder="Description*" onChangeText={(text)=> {this.updateState({description: text})}}
                                       underlineColorAndroid={nullIfUndefined(this.validateField('description'), '#ed2f2f')} style={styles.defaultTextInput} placeholderTextColor='#575757'
                                       ref="_descriptionInput"/>
                            <ErrorMessage text={this.validateField('description')}/>
                        </View>
                    </ListItem>
                    <ListItem itemDivider>
                        <Text>Contact info</Text>
                    </ListItem>
                    <ListItem style={styles.input}>
                        <InputGroup disabled>
                            <Icon name='md-person' style={styles.createIcon}/>
                            <Input placeholder='Name' value={this.state.newArticle.author}/>
                        </InputGroup>
                    </ListItem>
                    <ListItem itemDivider>
                        <Text>Location</Text>
                    </ListItem>
                    <CurrentLocations addresses={this.state.addresses} location={this.state.newArticle.location} onSelect={(place) => this.updateState({location: place})} />
                    <ArticlePlacePicker addresses={this.state.addresses} location={this.state.newArticle.location} onPlaceSelected={(place) => this.updateState({location: place})} />
                    <SelectedLocation location={this.state.newArticle.location ? this.state.newArticle.location.name : ''} onClose={() => this.pickLocation(undefined)}/>
                    <ListItem itemDivider>
                        <Text>Tags</Text>
                    </ListItem>
                    <ListItem>
                        <InputTag value={this.state.newArticle.tags}
                                  onChangeTags={(tags) => this.updateState({tags: tags})}/>
                    </ListItem>
                </List>
                <Button onPress={this.submit} title="submit" block>Submit</Button>
            </Content>
        </Container>
    }

    // ------ DIALOGS AND ALERTS ------
    showDialog = (options) => {
        var dialog = new DialogAndroid();
        dialog.set(options);
        dialog.show();
    };

    showSuccess = () => {
        var options = {
            title: 'Success',
            content: 'New article was successfully added',
            positiveText: 'Go to List',
            negativeText: 'Go to My Articles',
            onPositive: this.returnToList,
            onNegative: this.returnToUserArticles
        };
        this.showDialog(options)
    };

    showError = (err) => {
        var errorText = JSON.stringify(err).message;
        var options = {
            title: 'Error',
            content: 'An error occurred: ' + errorText,
            positiveText: 'OK'
        };
        this.showDialog(options)
    };

}

function mapStateToProps(state) {
    return state;
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateScreen);