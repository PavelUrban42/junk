import React, {Component, PropTypes} from "react";
import {TextInput, View, TouchableOpacity, ScrollView} from "react-native";
import {Container, Content, List, ListItem, InputGroup, Input, Icon, Text, Button} from "native-base";
import JunkCamera from "../../components/JunkCamera";
import InputTag from "../../components/tags/InputTag";
import SelectedLocation from "../../components/article-form/SelectedLocation";
import ErrorMessage from "../../components/article-form/ErrorMessage";
import ArticlePlacePicker from "../../components/article-form/ArticlePlacePicker";
import CurrentLocations from "../../components/article-form/CurrentLocations";
import DialogAndroid from "react-native-dialogs";
import styles from "../../styles/articles/styles";
import {ActionCreators} from "../../actions/index";
import bindActionCreators from "redux/es/bindActionCreators";
import connect from "react-redux/src/components/connect";
import PlacesGeocoder from "../../utils/placesGeocoder";
import {Actions} from "react-native-router-flux";
import {isNotEmpty, validate} from "../../utils/validator";
import {nullIfUndefined, getCurrentPosition, logError} from "../../utils/defaults";

class EditScreen extends Component {

    static propTypes = {
        user: PropTypes.object.isRequired
    };

    componentWillMount() {
        var validationRules = {
            topic: [{
                func: isNotEmpty,
                errorMessage: 'Field is required'
            }],
            description: [{
                func: isNotEmpty,
                errorMessage: 'Field is required'
            }]
        };
        let article = this.props.articles.currentArticle;
        this.setState({article: article, addresses: [], validationRules});
    }

    componentDidMount() {
        if (!this.state.article.location) {
            getCurrentPosition(this.updateAddresses, logError);
        }
    }
    // ------- update state block ------
    updateAddresses = (latitude, longitude) => {
        PlacesGeocoder.getPlaces(latitude, longitude).then(res => {
            this.setState({
                addresses: res || []
            });
        }).catch(logError);
    };

    updateState = (obj) => {
        let article = Object.assign(this.state.article, obj);
        this.setState({article});
    };

    // ----- COMPONENT ACTIONS -----

    resetSelectedLocation = () => {
        this.updateState({location: undefined});
        if (this.state.addresses.length == 0) {
            getCurrentPosition(this.updateAddresses, logError);
        }
    };

    onImageSelected = (url) =>  {
        let images = this.state.article.images || [];
        this.updateState({images: [...images, url]});
    };

    focusTo = (field) =>  {
        if (this.refs[field]) {
            this.refs[field].focus(); // RN input
        } else if (this[field]) {
            this[field]._textInput.focus(); // nativebase input
        }
    };

    validateField = (field) => {
        return validate(this.state.validationRules, this.state.article, field);
    };

    setDefaultValues = () => {
        let article = this.state.article;
        Object.keys(this.state.validationRules).forEach(key => {
            if (article[key] === undefined) {
                article[key] = '';
            }
        });
        this.setState({article});
    };

    submit = () => {
        this.setDefaultValues();
        var invalidKeys = Object.keys(this.state.validationRules).map(key => {
            if (this.validateField(key)) {
                return key;
            }
        }).filter(key => key != undefined);
        if (invalidKeys.length > 0) {
            var fieldname = "_" + invalidKeys[0] + "Input";
            this.focusTo(fieldname); // focus to first invalid field
            return;
        }
        let article = this.state.article;
        if (this.state.article.location) {
            let location = {
                formattedAddress: article.location.formattedAddress,
                geo: {
                    latitude: article.location.location.lat,
                    longitude: article.location.location.lng
                },
                name: article.location.name
            };

            article = Object.assign(this.state.article, location);
        }
        this.props.confirmEditArticle(article)
            .then(() => {Actions.pop();Actions.refresh();})
            .catch(this.showError);
    };

    render() {
        const {
            topic,
            description,
            images
        } = this.state.article;
        let convertedImages = images.map(image => {
            return {isStatic: true, uri: image}
        });

        return <Container style={styles.createContainer}>
            <Content>
                <JunkCamera onImageSelected={imageUrl => this.onImageSelected(imageUrl)} images={convertedImages}/>
                <List>
                    <ListItem style={styles.input}>
                        <View style={styles.validatedInput} refreshing>
                            <InputGroup error={this.validateField('topic')}>
                                <Input placeholder="Topic*" value={topic} onChangeText={(text)=> { this.updateState({topic : text})}} ref={c => this._topicInput = c}/>
                            </InputGroup>
                            <ErrorMessage text={this.validateField('topic')}/>
                        </View>
                    </ListItem>
                    <ListItem style={styles.input}>
                        <View style={styles.validatedInput} refreshing>
                            <TextInput multiline numberOfLines={3} placeholder="Description*" onChangeText={(text)=> {this.updateState({description: text})}}
                                       value={description}
                                       underlineColorAndroid={nullIfUndefined(this.validateField('description'), '#ed2f2f')} style={styles.defaultTextInput} placeholderTextColor='#575757'
                                       ref="_descriptionInput"/>
                            <ErrorMessage text={this.validateField('description')}/>
                        </View>
                    </ListItem>
                    <ListItem itemDivider>
                        <Text>Contact info</Text>
                    </ListItem>
                    <ListItem style={styles.input}>
                        <InputGroup disabled>
                            <Icon name='md-person' style={styles.createIcon}/>
                            <Input placeholder='Name' value={this.state.article.author}/>
                        </InputGroup>
                    </ListItem>
                    <ListItem itemDivider>
                        <Text>Location</Text>
                    </ListItem>
                    <CurrentLocations addresses={this.state.addresses} location={this.state.article.location} onSelect={(place) => this.updateState({location: place})} />
                    <ArticlePlacePicker addresses={this.state.addresses} location={this.state.article.location} onPlaceSelected={(place) => this.updateState({location: place})} />
                    <SelectedLocation location={this.state.article.location ? this.state.article.location.name : ''} onClose={() => this.resetSelectedLocation()}/>
                    <ListItem itemDivider>
                        <Text>Tags</Text>
                    </ListItem>
                    <ListItem>
                        <InputTag value={this.state.article.tags} onChangeTags={(tags) => this.updateState({tags: tags})}/>
                    </ListItem>
                </List>
                <Button onPress={this.submit} title="submit" block>Submit</Button>
            </Content>
        </Container>
    }

    // ------ DIALOGS AND ALERTS ------
    showDialog = (options) => {
        var dialog = new DialogAndroid();
        dialog.set(options);
        dialog.show();
    };

    showError = (err) => {
        var errorText = JSON.stringify(err).message;
        var options = {
            title: 'Error',
            content: 'An error occurred: ' + errorText,
            positiveText: 'OK'
        };
        this.showDialog(options)
    };

}

function mapStateToProps(state) {
    return state;
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(EditScreen);
