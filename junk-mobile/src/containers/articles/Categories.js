//noinspection JSUnresolvedVariable
import React, {Component, PropTypes} from "react";
import {ListItem, Text, Thumbnail} from "native-base";
import {connect} from "react-redux";

class Categories extends Component {

    static contextTypes = {
        routes: PropTypes.object.isRequired,
    };


    render() {
        const {routes} = this.context;
        let {
            description,
            id,
            images,
            location,
            phone,
            tags,
            topic
        } = this.props.data;

        return (
            <Container>
                <Content>
                    <List>
                        <ListItem>
                            <Text>Kumar Pratik</Text>
                            <Text note>Doing what you like will always keep you happy . .</Text>
                        </ListItem>
                        <ListItem>
                            <Text>Kumar Sanket</Text>
                            <Text note>Life is one time offer! Use it well</Text>
                        </ListItem>
                    </List>
                </Content>
            </Container>
        )
    }
}


function mapStateToProps(state) {
    return state;

}

export default connect(mapStateToProps)(Categories);
