//noinspection JSUnresolvedVariable
import React, {Component, PropTypes} from "react";
import {Dimensions, View, Image, TextInput} from "react-native";
import {Container, Content, Text, Button, List, ListItem, Header} from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import Swiper from "react-native-swiper";
import JunkTags from "../../components/tags/JunkTag";
import Comment from "../../components/Comment";
import styles from "../../styles/articles/styles";
import connect from "react-redux/src/components/connect";
import {ActionCreators} from "../../actions/index";
import bindActionCreators from "redux/es/bindActionCreators";
const {height, width} = Dimensions.get('window');
import {Actions as scenes} from "react-native-router-flux";

class DetailsScreen extends Component {

    componentWillMount() {
        this.updateState();
    }

    updateState = () => {
        let article = this.props.articles.currentArticle;
        let userComment = article.comments.find(comment => comment.user == this.props.user.profile.id);
        const {
            name,
            id,
            picture
        } = this.props.user.profile;

        this.setState({
            newComment: {
                article: article.id,
                author: name,
                user: id,
                userPicture: picture
            },
            userComment: userComment,
            userOwnArticle: this.props.user.profile.id == article.user
        });
    };

    updateComment = (text) => {
        var comment = this.state.newComment;
        this.setState({newComment: Object.assign({}, comment, {text: text})});
    };


    pickUpItem = () => {
        this.props.addComment(this.state.newComment).then(() => {
            this.updateState();
        });
    };

    _renderPickupButton() {
        if (!this.state.userOwnArticle && this.state.userComment === undefined) {
            return (
                <List style={{marginTop: 10}}>
                    <ListItem>
                        <TextInput multiline numberOfLines={3} placeholder="Comment" placeholderTextColor='#575757'
                                   style={{
                                       borderWidth: 0.5,
                                       borderColor: '#575757'
                                   }}
                                   onChangeText={(text) => this.updateComment(text)}/>
                    </ListItem>
                    <Button title="pickUp" onPress={this.pickUpItem} block> Pick Up </Button>
                </List>
            )
        } else {
            return null;
        }
    }

    _renderEditButton() {
        if (this.state.userOwnArticle) {
            let user = this.props.user;
            let article = this.state.article;
            return (
                <Button style={styles.iconButton} transparent onPress={() => scenes.articleEdit({user,article})} title="edit"><Icon name="edit" style={styles.detailsIcon}/></Button>
            )
        }
        return null;
    }

    _renderDeleteButton() {
        if (this.state.userOwnArticle) {
            return (
                <Button style={styles.iconButton} transparent onPress={() => console.log('remove')} title="remove"><Icon name="remove" style={styles.detailsIcon}/></Button>
            )
        }
        return null;
    }

    render() {
        const {
            author,
            description,
            images,
            tags,
            topic,
            user,
            comments
        } = this.props.articles.currentArticle;

        return (
            <Container>
                <Header style={styles.detailsHeader}>
                    <View style={styles.rowFlex} refreshing>
                        <Text style={styles.detailsHeaderText}>{topic}</Text>
                        {this._renderEditButton()}
                        {this._renderDeleteButton()}
                    </View>
                </Header>
                <Content>
                    {
                        images.length > 0 ?
                            <Swiper height={height / 2} horizontal={true} showButtons>
                                {images.map((image, i) => {
                                    return (
                                        <View style={{width: width, height: height / 2}} key={i} refreshing>
                                            <Image style={{width: width, height: height / 2}} source={{uri: image}}
                                                   key={image}/>
                                        </View>)
                                })}
                            </Swiper>
                            : null
                    }

                    <List>
                        <ListItem>
                            <Text style={styles.detailsText}>
                                {description}
                            </Text>
                        </ListItem>

                        <ListItem>

                            <Text style={styles.detailsText}> <Icon name='user' style={styles.detailsIcon}/> {author}
                            </Text>
                        </ListItem>

                        <JunkTags
                            style={styles.input}
                            data={tags}
                        />
                        <ListItem itemDivider>
                            <Text>Requests:</Text>
                         </ListItem>
                        {
                            comments.map(comment => {
                                return <Comment data={comment} key={comment._id}/>
                            })
                        }
                    </List>
                    {this._renderPickupButton()}
                </Content>
            </Container>
        );
    }
}
function mapStateToProps(state) {
    return state;
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(DetailsScreen);