//noinspection JSUnresolvedVariable
import React, {Component, PropTypes} from "react";
import {View} from "react-native";
import {Container, Content, List, Text, Button, InputGroup, Input, Header} from "native-base";
import {connect} from "react-redux";
import {ActionCreators} from "../../actions";
import {bindActionCreators} from "redux";
import Article from "../../components/ArticleItem";
import ArticlesMap from "../../components/location/ArticlesMap";
import Icon from "react-native-vector-icons/FontAwesome";
import styles from "../../styles/articles/styles";

class ListScreen extends Component {

    static contextTypes = {
        routes: PropTypes.object.isRequired,
    };

    constructor(props) {
        super(props);
        this.state = {
            searching: false,
            searchInput: ""
        };
    }

    getArticles() {
        this.setState({searching: true});
        this.props.loadArticles(this.state.searchInput).then(() => {
            this.setState({searching: false})
        });
    }

    searchArticles() {
        this.setState({searching: true});
        this.props.searchArticles(this.state.searchInput, this.props.articles.page).then(() => {
            this.setState({searching: false})
        });
    }

    onScroll(event) {
        const offset = event.nativeEvent.contentOffset.y;
        const screen = event.nativeEvent.layoutMeasurement.height;
        const content = event.nativeEvent.contentSize.height;
        if ((offset + screen >= content - screen / 2) && !this.state.searching &&
            this.props.articles.canLoadMore
        ) {
            console.log(this.state.searching);
            console.log("trigger");
            this.searchArticles();
        }
    }

    static addArticle(routes) {
        routes["article-new"]();
    }

    articles() {
        let searchedArticles = this.props.articles.items;
        return Object.keys(searchedArticles).map(key => searchedArticles[key]);
    }

    componentDidMount() {
        this.getArticles();
    }

    render() {
        const {routes} = this.context;

        return (
            <Container>
                <Header style={styles.listHeader}>
                    <View style={styles.rowFlex} refreshing>
                            <InputGroup style={styles.listInputGroup} >
                                <Input info
                                       placeholder='Type topic or related keyword here...'
                                       onChangeText={(searchInput) => this.setState({searchInput})}
                                       value={this.state.searchInput}
                                />
                                <Button title="Search" transparent style={styles.listButton}
                                        onPress={() => {
                                            this.getArticles()
                                        }}>
                                    <Icon name='search' style={styles.listIcon}/>
                                </Button>
                            </InputGroup>

                            <Button title="Add" bordered rounded style={[styles.listButton, styles.iconButton]}
                                    onPress={ListScreen.addArticle.bind(this, routes)}>
                                <Icon name='plus' style={styles.listIcon}/>
                            </Button>
                    </View>
                </Header>
                <Content onScroll={this.onScroll.bind(this)} style={styles.listContainer}>

                    {!this.state.searching ? <ArticlesMap data={this.articles()}/> : null}
                    <List>
                        {this.articles().map((article) => {
                            return <Article data={article} key={article.id}/>;
                        })}
                        {this.state.searching ? <Text>Searching...</Text> : null }
                    </List>
                </Content>
            </Container>
        );
    }
}

function mapStateToProps(state) {
    return state;
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ListScreen);