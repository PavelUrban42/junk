//noinspection JSUnresolvedVariable
import React, {Component} from "react";
import Routes from "../routes";
import {ActionCreators} from "../actions";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

class App extends Component {
    constructor(props) {
        super(props);
    };

    render() {
        return (
            <Routes />
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(() => {
    return {}
}, mapDispatchToProps)(App);