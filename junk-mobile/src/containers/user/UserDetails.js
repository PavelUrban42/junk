//noinspection JSUnresolvedVariable
import React, {Component, PropTypes} from "react";
//noinspection JSUnresolvedVariable
import {
    StyleSheet,
    Dimensions,
    NativeModules,
    View,
    Image
} from "react-native";
//noinspection JSUnresolvedVariable
import {GoogleSignin} from 'react-native-google-signin';
import {Container, Content, Text, Button} from 'native-base';
import {connect} from "react-redux";
import {ActionCreators} from "../../actions";
import {bindActionCreators} from "redux";

import {loginMethods} from "../../utils/loginMethods";

class UserDetails extends Component {
    static contextTypes = {
        routes: PropTypes.object.isRequired
    };
    constructor(props) {
        super(props);
    }

    onPressLogout = () => {
        const {routes} = this.context;
        GoogleSignin.signOut().then(() => {
            this.props.logoutUser();
        }).then(()=>{
            routes['article-login']();
        });
    };

    onPressOpenList = () => {
        const {routes} = this.context;
        routes['articleList']();
    };

    render() {
        console.log('UserDetails render', this.props);

        return (
            <Container style={styles.container}>
                <Content style={styles.content}>
                    <Image style={styles.userIcon} source={{uri: this.props.user.profile.picture}}/>
                    <Text style={styles.text}>{this.props.user.profile.name}</Text>
                    <Text style={styles.text}>{this.props.user.profile.email}</Text>

                    <Button
                        rounded primary
                        style = {styles.button}
                        onPress={this.onPressOpenList}>
                        Open list
                    </Button>
                    {this.props.user.service === loginMethods.DEVICE ? null :
                        <Button
                                rounded danger
                                style={styles.button}
                                onPress={this.onPressLogout}>
                            Log out
                        </Button>
                    }
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    userIcon: {
        marginTop: 16,
        width: 268,
        height: 230
    },
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 0
    },
    content: {
        marginTop: 55
    },
    text: {
        alignSelf: 'center',
        fontSize: 16
    },
    button: {
        marginTop: 20,
        width: 268,
        height: 48
    }
});

UserDetails.propTypes = {
    user: PropTypes.object,
    authToken: PropTypes.string
};


function mapStateToProps(state) {
    return state;
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(UserDetails);
