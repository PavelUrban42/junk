import React, {PropTypes, Component} from "react";
import {StyleSheet,View} from "react-native";
import Config from "react-native-config";
import {GooglePlacesAutocomplete} from "react-native-google-places-autocomplete";

export default class PlacePicker extends Component {

    static propTypes = {
        addresses: PropTypes.array,
        onPlaceSelected: PropTypes.func
    };

    pickAddress(details) {
        let add = {
            location: details.geometry.location,
            formattedAddress: details.formatted_address || details.description,
            name: details.name || details.formatted_address || details.description
        };
        this.props.onPlaceSelected(add);
    }

    render() {
        return <GooglePlacesAutocomplete
            minLength={2}
            placeholder={'Enter Location'}
            placeholderTextColor='#575757'
            autoFocus={false}
            fetchDetails={true}
            styles={styles}
            query={{
                key: Config.GOOGLE_API_KEY,
                language: 'en' // language of the results
            }}
            onPress={(data, details = null) => (this.pickAddress(details))}
            currentLocation={false}
            predefinedPlaces={this.formatAddresses()}
        />
    }

    formatAddresses() {
        return this.props.addresses.map((address) => {
            return {
                description: address.name,
                geometry: {
                    location: address.location
                }
            };
        });
    }

}

const styles = StyleSheet.create({
    textInputContainer: {
        backgroundColor: 'rgba(0,0,0,0)',
        borderTopWidth: 0,
        borderBottomWidth: 0
    },
    textInput: {
        marginLeft: 0,
        marginRight: 0,
        height: 38,
        color: '#5d5d5d',
        fontSize: 16
    },
    predefinedPlacesDescription: {
        color: '#1faadb'
    },
    powered: {
        width: 0,
        height: 0
    }
});
