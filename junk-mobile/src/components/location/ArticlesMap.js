//noinspection JSUnresolvedVariable
import React, {Component} from "react";
import {StyleSheet, Dimensions,Text,View} from "react-native";
import MapView from "react-native-maps";
import {Content, Card, CardItem} from "native-base";
const {width, height} = Dimensions.get('window');

const ASPECT_RATIO = width / height;
// Minsk coordinates
const LATITUDE = 53.9030611;
const LONGITUDE = 27.5583053;
const LATITUDE_DELTA = 0.15;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const CONTAINER_WIDTH = width - 30;

export default class ArticlesMap extends Component {

    state = {
        currentRegion: {
            latitude: LATITUDE,
            longitude: LONGITUDE,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA
        }
    };

    onRegionChangeComplete(newRegion) {
        this.setState({currentRegion: newRegion});
    }

    render() {
        let data = this.props.data;
        if (data && data.length > 0) {
            let markers = this.props.data.map((article, i) => {
                if (article.location && article.location.geo && article.location.geo.longitude && article.location.geo.longitude) {
                    let coords = {
                        latitude: article.location.geo.latitude,
                        longitude: article.location.geo.longitude,
                        latitudeDelta: this.state.currentRegion.latitudeDelta,
                        longitudeDelta: this.state.currentRegion.longitudeDelta
                    };
                    return (<MapView.Marker key={i} coordinate={coords} title={article.description} description={article.location.formattedAddress}>

                    </MapView.Marker>);
                } else {
                    return null;
                }
            });

            return (<Content containerStyle={styles.container}>
                <Card refreshing style={styles.mapContainer}>
                    <CardItem>
                        <MapView
                            initialRegion={this.state.currentRegion}
                            onRegionChangeComplete={(region) => this.onRegionChangeComplete(region)}
                            style={styles.map}>
                            {markers}
                        </MapView>
                    </CardItem>
                </Card>
            </Content>);
        } else {
            return null;
        }
    }
}
const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-start',
        alignItems: 'center',
        bottom: 10,
        right: 10,
        left: 10,
        top: 10
    },
    mapContainer: {
        bottom: 10
    },
    map: {
        height: height / 2,
        width: CONTAINER_WIDTH
    },
    article: {
        fontSize: 14
    }
});