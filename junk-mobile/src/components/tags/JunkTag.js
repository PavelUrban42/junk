import React, {Component, PropTypes} from "react";
import {View, Text, TextInput, ScrollView} from "react-native";
import {Button} from "native-base";
import styles from '../../styles/tags/styles';

export default class JunkTags extends Component {

    static propTypes = {
        data: PropTypes.array.isRequired,
        onTagSelected: PropTypes.func
    };

    pickTag(item) {
        if (this.props.onTagSelected) {
            this.props.onTagSelected(item);
        }
    }

    getItemValue(item) {
        return (typeof this.props.outputField != 'undefined') ?
                item[this.props.outputField] :
                item;
    }

    render() {
        let outputField = this.props.outputField;
        let items = this.props.data;
        if (typeof outputField != 'undefined') {

        }
        return items.length == 0 ? null : <ScrollView horizontal style={styles.tagContainer} refreshing showsHorizontalScrollIndicator={false}>
            {items.map((item, i) => {
                return <Button key={i} style={styles.tagView} transparent onPress={() => this.pickTag(item)}
                               title="pickTag">
                    {this.getItemValue(item)}
                </Button>
            })}
        </ScrollView>;
    }
}