import React, {Component, PropTypes} from "react";
import {View, Text, TextInput, ScrollView, TouchableOpacity} from "react-native";
import styles from '../../styles/tags/styles';

export default class InputTag extends Component {

    static propTypes = {
        onChangeTags: PropTypes.func,
        value: PropTypes.array
    };

    componentWillMount() {
        this.setState({
            text: '',
            tags: this.props.value || []
        });
    }

    onChangeText(text) {
        if (text.length > 1 && (text.slice(-1) === ' ' || text.slice(-1) === ',')) {
            let newTag = text.slice(0, -1).trim();
            if (newTag.length > 0 && this.state.tags.indexOf(newTag) === -1) {
                if (newTag.indexOf(' ') >= 0) {
                    newTag = newTag.split(' ').filter((item) => {
                        return item != ''
                    }).join(' ');
                }
                this.updateTags([...this.state.tags, newTag], '');
            } else {
                this.setState({text});
            }
        } else {
            this.setState({text});
        }
    }

    removeTag(i) {
        let tags = this.state.tags;
        tags.splice(i,1);
        this.updateTags(tags, this.state.text);
    }

    updateTags(tags, text) {
        this.setState({
            tags: tags,
            text: text,
        });
        this.props.onChangeTags(tags);
    }

    render() {
        return (<ScrollView horizontal style={styles.tagContainer} refreshing showsHorizontalScrollIndicator={false}>
            {this.state.tags.map((item, i) => {
                return <View key={i} style={[styles.tagView, styles.tagContainer]} refreshing>
                    <TouchableOpacity onPress={() => this.removeTag(i)}><Text style={styles.tagText}>{item}&nbsp;&times;</Text></TouchableOpacity>
                </View>
            })}
            <View style={[styles.tagView, styles.tagContainer, {minWidth: 150}]} refreshing>
            <TextInput
                    value={this.state.text}
                    onChangeText={(text) => this.onChangeText(text)}
                    underlineColorAndroid="transparent"
                    placeholder="Enter tags"
                    style={styles.tagTextInput}
                />
                </View>
        </ScrollView>)
    }
}
