import React, {Component} from "react";
import {ScrollView, View, Text, TouchableOpacity} from "react-native";
import {ListItem} from "native-base";
import styles from "../../styles/articles/styles";
import {nullIfUndefined} from "../../utils/defaults";

export default class SelectedLocation extends Component {

    render() {
        return nullIfUndefined(this.props.location,
            <ListItem style={styles.input}>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                    <View style={[styles.locationContainer, styles.location]} refreshing>
                        <Text style={styles.locationText}>{this.props.location}</Text>
                        <TouchableOpacity onPress={() => this.props.onClose()}>
                            <Text style={styles.locationText}>&nbsp;&times;</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </ListItem>
        );
    }
}