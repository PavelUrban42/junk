import React, {Component} from "react";
import {ListItem} from "native-base";
import JunkTags from "../tags/JunkTag";
import styles from "../../styles/articles/styles";
import {nullIfDefined, nullIfEmpty} from "../../utils/defaults";

export default class CurrentLocations extends Component {

    render() {
        return nullIfEmpty(this.props.addresses,
            nullIfDefined(this.props.location,
                <ListItem style={styles.input}>
                    <JunkTags style={styles.input} data={this.props.addresses} outputField="name" onTagSelected={(tag) => this.props.onSelect(tag)}/>
                </ListItem>));
    }
}