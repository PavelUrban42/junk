import React, {Component} from "react";
import {ListItem} from "native-base";
import PlacePicker from "../location/PlacePicker";
import styles from "../../styles/articles/styles";
import {nullIfDefined} from "../../utils/defaults";

export default class ArticlePlacePicker extends Component {

    render() {
        return nullIfDefined(this.props.location, (<ListItem style={styles.input}>
                <PlacePicker addresses={this.props.addresses} onPlaceSelected={(place) => this.props.onPlaceSelected(place)}/>
            </ListItem>));
    }

}