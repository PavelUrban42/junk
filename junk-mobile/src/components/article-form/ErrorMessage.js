import React, {Component} from "react";
import {Text} from "react-native";
import styles from "../../styles/articles/styles";
import {nullIfUndefined} from "../../utils/defaults";

export default class ErrorMessage extends Component {

    render() {
        return nullIfUndefined(this.props.text,
            <Text style={styles.validationError}>{this.props.text}</Text>
        );
    }
}