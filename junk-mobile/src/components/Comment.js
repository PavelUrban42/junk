import React, {Component, PropTypes} from "react";
import {ListItem, Text, Thumbnail } from "native-base";

export default class Comment extends Component {

    static propTypes = {
        data: PropTypes.object.isRequired,
    };

    convertDate(datetime) {
        const options = {
            year: 'numeric',
            month: 'long',
            day: 'numeric',
            hour: 'numeric',
            minute: 'numeric'

        };
        return new Date(datetime).toLocaleString("en-US", options);
    };

    render(){
        const {
            text,
            author,
            userPicture,
            createdAt
        } = this.props.data;
        return (<ListItem>
            <Thumbnail square size={80} source={{uri : userPicture}} />
            <Text>{author}</Text>
            <Text note>at {this.convertDate(createdAt)}</Text>
            <Text>{text}</Text>
        </ListItem>);
    }
}