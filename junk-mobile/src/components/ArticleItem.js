//noinspection JSUnresolvedVariable
import React, {Component, PropTypes} from "react";
import {ListItem, Text, Thumbnail } from "native-base";
import {connect} from "react-redux";
import JunkTags from "./tags/JunkTag";
import {Actions, ActionConst} from "react-native-router-flux";
import {ActionCreators} from "../actions/index";
import {bindActionCreators} from "redux";

class ArticleItem extends Component {

    showDetails = (data) => {
        this.props.getArticle(data).then(() =>
            Actions.articleDetails()
        )
    };

    convertDate(datetime) {
        const options = {
            year: 'numeric',
            month: 'long',
            day: 'numeric',
            timezone: 'UTC',
            hour: 'numeric',
            minute: 'numeric'

        };
        return new Date(datetime).toLocaleString("en-US", options);

    };

    selectTag(tag) {
        if (this.props.onTagSelected) {
            this.props.onTagSelected(tag);
        }
    }

    render() {
        let {
            description,
            id,
            images,
            location,
            phone,
            tags,
            topic,
            createdAt,
            updatedAt
        } = this.props.data;

        return <ListItem onPress={() => this.showDetails(this.props.data)}>
            <Thumbnail square size={80} source={{uri : images[0]}} />
            <Text>{topic}</Text>
            <Text note>{description}</Text>
            <Text note>Created: {this.convertDate(createdAt)}</Text>
            <JunkTags
                data={tags}
                onTagSelected={(tag) => this.selectTag(tag)}
            />
        </ListItem>;

    }
}


function mapStateToProps(state) {
    return state;
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(ArticleItem);
