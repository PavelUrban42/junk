import React, {PropTypes} from "react";
import {Image, Platform, TouchableOpacity, Dimensions, View} from "react-native";
import {Container, Content} from "native-base";
import Swiper from "react-native-swiper";
import ImagePicker from "react-native-image-picker";
import styles from "../styles/camera/styles";
import ImageUploader from "../utils/imageUploader";

const {height} = Dimensions.get('window');

const max_size = 10;

export default class JunkCamera extends React.Component {

    static propTypes = {
        maxSize: PropTypes.number,
        images: PropTypes.array,
        onImageSelected: PropTypes.func
    };

    componentWillMount() {
        if (this.props.images) {
            this.setState({avatarSources: this.props.images});
        }
        if (this.props.maxSize) {
            this.setState({max: this.props.maxSize});
        } else {
            this.setState({max: max_size})
        }
    }

    state = {
        avatarSources: [],
        index: 0
    };

    selectPhotoTapped() {
        // todo image edit/delete/view
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };

        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled photo picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                console.log('uploading image');
                ImageUploader.uploadImage(response.fileName, response.uri)
                    .then(this.updateImage)
                    .catch(err => console.log(err));

            }

        });
    }

    updateImage = (url) => {
        var source;
        if (Platform.OS === 'android') {
            source = {uri: url, isStatic: true};
        } else {
            source = {uri: url.replace('file://', ''), isStatic: true};
        }
        this.setState({
            avatarSources: [...this.state.avatarSources, source],
            index: this.state.avatarSources.length
        });
        this.props.onImageSelected(url);
        this.refs.swiper.onScrollEnd({
            nativeEvent: {
                position: this.state.index
            }
        });
    };

    _renderImages() {
        let images = this.state.avatarSources.map((source, i) => {
            return <View style={styles.avatarContainer} key={i} refreshing>
                <Image style={styles.image} source={source}/>
            </View>
        });
        if (this.state.max > this.state.avatarSources.length) {
            return [...images, this._renderAddImage()];
        }
        return images;
    }

    _renderAddImage() {
         return <View style={styles.avatarContainer} key="-1" refreshing>
                <TouchableOpacity style={styles.image} onPress={()=> this.selectPhotoTapped()}>
                    <Image resizeMode="contain" style={styles.image} source={require('../../images/camera-icon.png')}/>
                </TouchableOpacity>
            </View>;

    }

    render() {
        return (
            <Container>
                <Content style={styles.container}>
                    <Swiper horizontal height={height / 2} ref="swiper" loop={false}>
                        {this._renderImages()}
                    </Swiper>
                </Content>
            </Container>
        );
    }
}