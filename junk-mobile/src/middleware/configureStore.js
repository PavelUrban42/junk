//noinspection JSUnresolvedVariable
import { createStore, applyMiddleware, combineReduxers, compose } from "redux";
import  thunkMiddleware from "redux-thunk";
import reducers from "../reducers/index"
import loggerMiddleware from "./logger";

function configureStore(initialState) {
    const enhancer = compose(
        applyMiddleware(
            thunkMiddleware,
            loggerMiddleware
        )
    );
    return createStore(reducers, initialState, enhancer);
}

export default configureStore;
