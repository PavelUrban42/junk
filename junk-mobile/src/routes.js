//noinspection JSUnresolvedVariable
import React, { Component } from "react";
import {Router, Scene, Actions } from "react-native-router-flux";
import ArticlesList from "./containers/articles/List";
import ArticleDetails from "./containers/articles/Details";
import ArticleLogin from "./containers/login/Login";
import UserDetails from "./containers/user/UserDetails"
import ArticleCreate from "./containers/articles/Create";
import ArticleEdit from "./containers/articles/Edit";

export default class AppRouter extends Component {

    render() {
        return (
            <Router>
                <Scene key="root">
                    <Scene
                        key="article-login"
                        component={ArticleLogin}
                        title="Authorization"
                        type="replace"
                        initial={true}
                    />
                    <Scene
                        key="profile"
                        component={UserDetails}
                        title="Profile"
                        type="replace"
                    />
                    <Scene
                        key="articleList"
                        component={ArticlesList}
                        title="Articles List"
                    />
                    <Scene
                        key="articleDetails"
                        component={ArticleDetails}
                        title="Article Details"
                        onBack={() => Actions.pop({ refresh: {}})}
                    />
                    <Scene
                        key="article-new"
                        component={ArticleCreate}
                        title="New Article"
                    />
                    <Scene
                        key="articleEdit"
                        component={ArticleEdit}
                        title="Edit Article"
                    />
                </Scene>
            </Router>
        );
    }
}