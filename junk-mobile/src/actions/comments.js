import * as types from "./types";
import service from "../services/articles";

export function createComment({article}) {
    return {
        type: types.UPDATE_ARTICLE,
        article
    };
}

export function addComment(comment) {
    return (dispatch) => {
        return service.addComment(comment.article,comment).then(response => {
            dispatch(createComment({
                article: response
            }));
        }).catch(ex => {
            console.log(ex);//todo;
        })
    };
}