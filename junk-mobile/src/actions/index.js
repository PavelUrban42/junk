import * as ArticleActions from "./articles";
import * as UserActions from "./users";
import * as CommentActions from "./comments";

export const ActionCreators = Object.assign({},
    ArticleActions, UserActions, CommentActions
);