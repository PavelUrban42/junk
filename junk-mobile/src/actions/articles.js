import * as types from "./types";
import service from "../services/articles";

export function createArticle({article}) {
    return {
        type: types.ADD_ARTICLE,
        article
    };
}
export function populateArticles({ articles }) {
    return {
        type: types.POPULATE_ARTICLES,
        articles,
    }
}

export function getArticles({ articles }) {
    return {
        type: types.GET_ARTICLES,
        articles
    }
}

export function updateArticle({article}) {
    return {
        type: types.UPDATE_ARTICLE,
        article
    };
}

export function pickUpArticle({article}) {
    return {
        type: types.PICKUP_ARTICLE,
        article
    };
}

export function addArticle(article) {
    return (dispatch) => {
        return service.addArticle(article).then(response => {
            dispatch(createArticle({
                article: response
            }));
        }).catch(ex => {
            console.log(ex);//todo;
        })
    };
}

export function searchArticles(terms, page) {
    return (dispatch) => {
        return service.listArticles(terms, page).then(response => {
            dispatch(populateArticles({
                articles: response
            }));
        }).catch(ex => {
            console.log(ex);//todo;
        })
    };
}

export function loadArticles(terms) {
    return (dispatch) => {
        return service.listArticles(terms).then(response => {
            dispatch(getArticles({
                articles: response
            }));
        }).catch(ex => {
            console.log(ex);//todo;
        })
    };
}

export function confirmEditArticle(article) {
    return (dispatch) => {
        return service.updateArticle(article).then(response => {
            dispatch(updateArticle({
                article: response
            }));
        }).catch(ex => {
            console.log(ex);//todo;
        })
    };
}

export function getArticle(article) {
    return (dispatch) => {
        return new Promise(function (resolve) {
            resolve(dispatch(pickUpArticle({article: article})));
        });
    }
}