import * as types from "./types";
import service from "../services/users";
import {loginMethods} from "../utils/loginMethods";

export function loginUser({user, service}) {
    return {
        type: types.LOGIN_USER,
        user,
        service
    }
}

export function logoutUser() {
    return{
        type: types.LOGOUT_USER
    }
}

export function authorizeUser(token) {
    return (dispatch) => {
        return service.authenticateUser(token).then((user) => {
            dispatch(loginUser({
                user,
                service: loginMethods.GOOGLE
            }));
        }).catch(ex => {
            console.log(ex);//todo;
        })
    };
}

export function getUserByDevice(deviceId) {
    return (dispatch) => {
        return service.getUserViaDevice(deviceId).then((user) => {
            if (user) {
                dispatch(loginUser({
                    user,
                    service: loginMethods.DEVICE
                }));
            }
        }).catch(ex => {
            console.log(ex);//todo;
        })
    };
}

export function authorizeUserViaDevice(user) {
    return (dispatch) => {
        return service.authenticateUserViaDevice(user).then((user) => {
            dispatch(loginUser({
                user,
                service: loginMethods.DEVICE
            }));
        }).catch(ex => {
            console.log(ex);//todo;
        })
    };
}