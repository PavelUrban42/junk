import { combineReducers } from "redux";
import * as articlesReducer from "./articles";
import * as usersReducer from "./users";

export default combineReducers(Object.assign(
    articlesReducer, usersReducer
));