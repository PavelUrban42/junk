import createReducer from "../utils/createReducer";
import * as types from "../actions/types";

const initialState = {
    token: "",
    profile: [],
    service: ""
};

export const user = createReducer(initialState, {
    [types.LOGIN_USER](state, action){
        return Object.assign({}, state, {
            token: action.user.token,
            profile: action.user.user,
            service: action.service
        });
    },
    [types.LOGOUT_USER](state, action){
        return Object.assign({}, state, {
            token: initialState.token,
            profile: initialState.profile
        })
    }
});
