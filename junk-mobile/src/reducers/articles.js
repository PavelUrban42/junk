import createReducer from "../utils/createReducer";
import * as types from "../actions/types";

export const articles = createReducer({
    items: {},
    page: 1,
    currentArticle: {}
}, {
    [types.POPULATE_ARTICLES](state, action) {
        let nextPage = action.articles.items.map((article) => {
            let id = article.id;
            return Object.assign({}, article, {id});
        });
        return {
            items: [...state.items,...nextPage],
            page: state.page + 1,
            canLoadMore: action.articles.canLoadMore
        };
    },
    [types.GET_ARTICLES](state, action) {
        let result = {
            items: action.articles.items,
            page: 1,
            canLoadMore: action.articles.canLoadMore
        };
        return result;
    },
    [types.ADD_ARTICLE](state, action) {
        let result = {
            currentArticle: action.article
        };
        return Object.assign(state, result);
    },
    [types.UPDATE_ARTICLE](state, action) {
        let updated = action.article;
        let result = state.items.map(article => {
            if (article.id != updated.id) {
                return article;
            }
            return updated;
        });
        return Object.assign(state, {items: result, currentArticle: updated});
    },
    [types.PICKUP_ARTICLE](state, action) {
        let current = Object.assign({},action.article);
        return Object.assign(state, {currentArticle: action.article});
    }
});