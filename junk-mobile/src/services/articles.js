import Api from "../utils/api";

export default class ArticlesService {

    static listArticles(terms, page = 1) {
        const params = [
            `s=${(terms)? encodeURIComponent(terms) : ""}`,
            `p=${page}`
        ].join('&');
        return Api.get(`/articles?${params}`);
    }

    static addArticle(article) {
        return Api.post(`/articles`, article);
    }

    static addComment(article,comment) {
        return Api.post(`/articles/${article}/comments`, comment);
    }

    static updateArticle(article) {
        return Api.put(`/articles/${article.id}`, article);
    }

}
