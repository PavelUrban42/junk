import Api from "../utils/api";

export default class UserService {
    static authenticateUser(authCode) {
        const params = [
            `access_token=${authCode}`
        ].join('&');
        return Api.post(`/auth/google?${params}`);
    }

    static authenticateUserViaDevice(user) {
        return Api.post(`/auth/device`, user);
    }

    static getUserViaDevice(deviceId) {
        return Api.get(`/auth/device/${deviceId}`);
    }
}
