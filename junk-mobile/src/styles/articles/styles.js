import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    // details
    detailsIcon: {
        fontSize: 20,
        color: '#000000'
    },
    detailsText: {
       fontSize: 16
    },
    detailsHeaderText: {
        fontSize: 20,
        marginTop: 5,
        marginBottom: 5,
        flex: 12
    },
    detailsHeader: {
        backgroundColor: '#d0d0d0',
        marginTop: 55,
        paddingLeft: 10
    },

    // create
    input: {
        marginRight: 10,
        marginLeft: 10,
        borderBottomWidth: 0,
        paddingTop: 5,
        paddingBottom: 5
    },
    createContainer: {
        marginTop: 55
    },
    createIcon: {
        color: '#384850'
    },
    locationContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    location: {
        backgroundColor: '#e0e0e0',
        borderRadius: 16,
        paddingLeft: 14,
        paddingRight: 14,
        height: 32,
        margin: 4,
        flex: 1
    },
    locationText: {
        fontSize: 15,
        color: 'rgba(0, 0, 0, 0.87)',
        textAlign: 'center',
        top: 4
    },
    validatedInput: {
        flexDirection: 'column',
    },
    validationError: {
        color: 'red',
        marginLeft: 10,
        fontSize: 12,
        lineHeight: 16
    },
    defaultTextInput: {
        fontSize: 15
    },

    // list
    listContainer: {
        marginLeft: 10,
        marginRight: 10
    },
    listIcon: {
        fontSize: 15,
        color: '#bebebe'
    },
    listButton: {
        height: 40,
        borderColor: "#DDDDDD"
    },
    listInputGroup: {
        height: 40,
        flex:8
    },
    listHeader : {
        backgroundColor: '#d0d0d0',
        marginTop: 55,
        paddingLeft: 0
    },

    // common
    rowFlex: {
        flexDirection: 'row'
    },
    iconButton: {
        flex: 1,
        left: 10
    }
});
