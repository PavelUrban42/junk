import { StyleSheet,Dimensions } from 'react-native';
const {height, width} = Dimensions.get('window');

export default StyleSheet.create({
    avatarContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        height: height/2,
        width: height/2,
        borderRadius: height/4
    },
    content: {
        height: height/2
    },
    container: {
        marginTop: 20,
        marginBottom: 20
    }
});
