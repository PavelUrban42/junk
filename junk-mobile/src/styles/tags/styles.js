import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    tagContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    tagTextInput: {
        height: 32,
        fontSize: 14,
        flex: 1,
        margin: 0,
        padding: 0,
        paddingLeft: 12,
        paddingRight: 12
    },
    tagText: {
        fontSize: 15,
        color: 'rgba(0, 0, 0, 0.87)',
        textAlign: 'center',
        top: 5
    },
    tagItemView: {
        fontSize: 13,
        color: 'rgba(0, 0, 0, 0.87)'
    },
    tagView: {
        justifyContent: 'center',
        backgroundColor: '#e0e0e0',
        borderRadius: 16,
        paddingLeft: 12,
        paddingRight: 12,
        height: 32,
        margin: 4,
        flex: 1
    }
});