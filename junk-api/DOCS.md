# junkapi v0.0.0



- [Article](#article)
	- [Create article](#create-article)
	- [Delete article](#delete-article)
	- [Retrieve article](#retrieve-article)
	- [Retrieve articles](#retrieve-articles)
	- [Update article](#update-article)
	
- [Auth](#auth)
	- [Authenticate with Google](#authenticate-with-google)
	
- [User](#user)
	- [Create user](#create-user)
	- [Delete user](#delete-user)
	- [Retrieve current user](#retrieve-current-user)
	- [Retrieve user](#retrieve-user)
	- [Retrieve users](#retrieve-users)
	- [Update user](#update-user)
	


# Article

## Create article



	POST /articles


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| topic			| 			|  <p>Article's topic.</p>							|
| description			| 			|  <p>Article's description.</p>							|
| images			| 			|  <p>Article's images.</p>							|
| tags			| 			|  <p>Article's tags.</p>							|
| author			| 			|  <p>Article's author.</p>							|
| phone			| 			|  <p>Article's phone.</p>							|
| location			| 			|  <p>Article's location.</p>							|
| comments			| 			|  <p>Article's comments.</p>							|

## Delete article



	DELETE /articles/:id


## Retrieve article



	GET /articles/:id


## Retrieve articles



	GET /articles


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update article



	PUT /articles/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| topic			| 			|  <p>Article's topic.</p>							|
| description			| 			|  <p>Article's description.</p>							|
| images			| 			|  <p>Article's images.</p>							|
| tags			| 			|  <p>Article's tags.</p>							|
| author			| 			|  <p>Article's author.</p>							|
| phone			| 			|  <p>Article's phone.</p>							|
| location			| 			|  <p>Article's location.</p>							|
| comments			| 			|  <p>Article's comments.</p>							|

# Auth

## Authenticate with Google



	POST /auth/google


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Google user accessToken.</p>							|

# User

## Create user



	POST /users


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Master access_token.</p>							|
| email			| String			|  <p>User's email.</p>							|
| name			| String			| **optional** <p>User's name.</p>							|
| picture			| String			| **optional** <p>User's picture.</p>							|
| role			| String			| **optional** <p>User's picture.</p>							|

## Delete user



	DELETE /users/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|

## Retrieve current user



	GET /users/me


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|

## Retrieve user



	GET /users/:id


## Retrieve users



	GET /users


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update user



	PUT /users/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|
| name			| String			| **optional** <p>User's name.</p>							|
| picture			| String			| **optional** <p>User's picture.</p>							|


