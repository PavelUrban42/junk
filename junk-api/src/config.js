/* eslint-disable no-unused-vars */
import path from 'path'
import _ from 'lodash'

/* istanbul ignore next */
const requireProcessEnv = (name) => {
	if (!process.env[name]) {
		throw new Error('You must set the ' + name + ' environment variable')
	}
	return process.env[name]
};

/* istanbul ignore next */
if (process.env.NODE_ENV !== 'production' && !process.env.CI) {
	const dotenv = require('dotenv-safe');
	dotenv.load({
		path: path.join(__dirname, '../.env'),
		sample: path.join(__dirname, '../.env.example')
	})
}

const config = {
	all: {
		env: process.env.NODE_ENV || 'development',
		root: path.join(__dirname, '..'),
		port: process.env.PORT || 9000,
		ip: process.env.IP || '0.0.0.0',
		masterKey: requireProcessEnv('MASTER_KEY'),
		jwtSecret: requireProcessEnv('JWT_SECRET'),
		awsConfig: {
			accessKeyId: requireProcessEnv('ACCESS_KEY_ID'),
			secretAccessKey: requireProcessEnv('SECRET_ACCESS_KEY'),
			region: requireProcessEnv('REGION'),
			bucket: requireProcessEnv('BUCKET'),
			folder: requireProcessEnv('FOLDER')
		},
		googleAuth: {
			clientID: "193791589708-brlk2ngalvoiqmq0b8t9353gmntpasc6.apps.googleusercontent.com",
			clientSecret: "p3-r6ZonCtbtlCtKXB7QMhra"
		},
		mongo: {
			options: {
				db: {
					safe: true
				}
			}
		}
	},
	test: {
		mongo: {
			uri: 'mongodb://localhost/junksharing-test',
			options: {
				debug: false
			}
		}
	},
	development: {
		mongo: {
			// Change for local or AWS DB
			uri: 'mongodb://localhost/junksharing-dev',
			//uri: 'mongodb://junk-user:junk@ec2-54-91-226-107.compute-1.amazonaws.com:27017/junksharing-dev',
			options: {
				debug: true
			}
		}
	},
	production: {
		ip: process.env.IP || undefined,
		port: process.env.PORT || 8080,
		mongo: {
			uri: process.env.MONGODB_URI || 'mongodb://localhost/junksharing'
		}
	}
};

module.exports = _.merge(config.all, config[config.all.env]);
export default module.exports
