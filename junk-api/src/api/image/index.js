import {Router} from 'express'
import {create} from './controller'
import multer from 'multer'

const router = new Router();

var upload = multer({storage: multer.memoryStorage()}).any(); // files will be stored in memory as Uint8Array

router.post('/',
	upload,
	create);

export default router;
