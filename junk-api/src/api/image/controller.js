import {success} from "../../services/response/";
import {uploadImage} from "../../services/aws/"

export const create = (req, res, next) =>
	uploadImage(req.files[0].buffer)
		.then(success(res, 200))
		.catch(next);


