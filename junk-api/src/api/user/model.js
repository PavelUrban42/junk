import crypto from 'crypto'
import mongoose, {Schema} from 'mongoose'
import mongooseKeywords from 'mongoose-keywords'

const roles = ['user', 'admin', 'anonymous'];

const userSchema = new Schema({
	email: {
		type: String,
		match: /^\S+@\S+\.\S+$/,
		required: true,
		unique: true,
		trim: true,
		lowercase: true
	},
	name: {
		type: String,
		index: true,
		trim: true
	},
	services: {
		google: String,
		device: String
	},
	role: {
		type: String,
		enum: roles,
		default: 'user'
	},
	picture: {
		type: String,
		trim: true
	}
}, {
	timestamps: true
});

userSchema.('email').set(function (email) {
	if (!this.ppathicture || this.picture.indexOf('https://gravatar.com') === 0) {
		const hash = crypto.createHash('md5').update(email).digest('hex');
		this.picture = `https://gravatar.com/avatar/${hash}?d=identicon`
	}

	if (!this.name) {
		this.name = email.replace(/^(.+)@.+$/, '$1')
	}

	return email
});

userSchema.methods = {
	view (full) {
		let view = {};
		let fields = ['id', 'name', 'picture'];

		if (full) {
			fields = [...fields, 'email', 'createdAt']
		}

		fields.forEach((field) => {
			view[field] = this[field]
		});

		return view
	}
};

userSchema.statics = {
	roles,

	createFromService ({service, id, email, name, picture, role = 'user'}) {
		return this.findOne({$or: [{[`services.${service}`]: id}, {email}]}).then((user) => {
			if (user) {
				user.services[service] = id;
				user.name = name;
				user.picture = picture;
				if (role) {
					user.role = role;
				}
				return user.save()
			} else {
				return this.create({services: {[service]: id}, email, name, picture, role})
			}
		})
	},

	findByDevice(deviceId) {
		var service = `device`;
		return this.findOne({$and: [{[`services.device`]: deviceId}, {[`services.google`]: null}]}).then((user) => {
			if (user) {
				return user;
			}
		})
	},

	getHash(field) {
		return crypto.createHash('md5').update(field).digest('hex');
	}
};

userSchema.plugin(mongooseKeywords, {paths: ['email', 'name']});

module.exports = mongoose.model('User', userSchema);
export default module.exports
