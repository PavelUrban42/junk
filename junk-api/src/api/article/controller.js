import _ from "lodash"
import {success, notFound} from "../../services/response/"
import {Article} from "./"

export const create = ({bodymen: {body}}, res, next) =>
	Article.create(body)
		.then((article) => article.view(true))
		.then(success(res, 201))
		.catch(next);

export const index = ({querymen, query}, res, next) => {
	const terms = (query.s || "").split(",").map((x) => new RegExp('^' + x.trim() + '$', "i"));
	const search = query.s ? {
		... querymen.query,
		$or: [{
			"topic": {$in: terms}
		}, {
			"tags": {$in: terms}
		}, {
			"images": {$in: terms}
		}]
	} : querymen.query;

	const cursor = {
		...querymen.cursor,
		skip: (query.p - 1) * querymen.cursor.limit
	};

	return Article.count(search).then((count) => {/*todo: fix error handling*/
		return Article.find(search, querymen.select, cursor)
			.then((articles) => {
				return {
					items: articles.map((article) => article.view()),
					canLoadMore: querymen.cursor.limit * query.p <= count
				};
			})
			.then(success(res))
			.catch(next);
	});
};


export const show = ({params}, res, next) =>
	Article.findById(params.id)
		.then(notFound(res))
		.then((article) => article ? article.view() : null)
		.then(success(res))
		.catch(next);

export const update = ({bodymen: {body}, params}, res, next) =>
	Article.findById(params.id)
		.then(notFound(res))
		.then((article) => article ? _.extend(article, body).save() : null)
		.then((article) => article ? article.view(true) : null)
		.then(success(res))
		.catch(next);

export const destroy = ({params}, res, next) =>
	Article.findById(params.id)
		.then(notFound(res))
		.then((article) => article ? article.remove() : null)
		.then(success(res, 204))
		.catch(next);

export const createComment = ({body, params}, res, next) => {
	Article.findById(params.id)
		.then(notFound(res))
		.then((article) => article ? _.extend(article, article.comments.push(body)).save() : null)
		.then((article) => article ? article.view(true) : null)
		.then(success(res, 201))
		.catch(next);
};
