import mongoose, {Schema} from 'mongoose'

const statuses = ['active', 'reserved', 'closed'];

const articleSchema = new Schema({
	topic: {
		type: String
	},
	description: {
		type: String
	},
	images: [{
		type: String
	}],
	tags: [{
		type: String
	}],
	author: {
		type: String
	},
	phone: {
		type: String
	},
	location: {
		type: Object
	},
	comments: [{
		text: String,
		author: String,
		user: Schema.Types.ObjectId,
		userPicture: String,
		createdAt: {type: Date, default: Date.now}
	}],
	user: {
		type: Schema.Types.ObjectId
	},
	status: {
		type: String,
		enum: statuses,
		default: 'active'
	}
}, {
	timestamps: true
});

articleSchema.methods = {
	view (full) {
		const view = {
			// simple view
			id: this.id,
			topic: this.topic,
			description: this.description,
			images: this.images,
			tags: this.tags,
			author: this.author,
			phone: this.phone,
			location: this.location,
			comments: this.comments,
			user: this.user,
			createdAt: this.createdAt,
			updatedAt: this.updatedAt
		};

		return full ? {
			...view
			// add properties for a full view
		} : view
	}
};

module.exports = mongoose.model('Article', articleSchema);
export default module.exports
