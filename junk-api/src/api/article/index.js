import {Router} from 'express'
import {middleware as query} from 'querymen'
import {middleware as body} from 'bodymen'
import {create, index, show, update, destroy, createComment} from './controller'
import {schema} from './model'
export Article, {schema} from './model'

const router = new Router();
const {topic, description, images, tags, author, phone, location, comments, user} = schema.tree;

/**
 * @api {post} /articles Create article
 * @apiName CreateArticle
 * @apiGroup Article
 * @apiParam topic Article's topic.
 * @apiParam description Article's description.
 * @apiParam images Article's images.
 * @apiParam tags Article's tags.
 * @apiParam author Article's author.
 * @apiParam phone Article's phone.
 * @apiParam location Article's location.
 * @apiParam comments Article's comments.
 * @apiSuccess {Object} article Article's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Article not found.
 */
router.post('/',
	body({topic, description, images, tags, author, phone, location, user}),
	create);

/**
 * @api {get} /articles Retrieve articles
 * @apiName RetrieveArticles
 * @apiGroup Article
 * @apiUse listParams
 * @apiSuccess {Object[]} articles List of articles.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
	query(),
	index);

/**
 * @api {get} /articles/:id Retrieve article
 * @apiName RetrieveArticle
 * @apiGroup Article
 * @apiSuccess {Object} article Article's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Article not found.
 */
router.get('/:id',
	show);

/**
 * @api {put} /articles/:id Update article
 * @apiName UpdateArticle
 * @apiGroup Article
 * @apiParam topic Article's topic.
 * @apiParam description Article's description.
 * @apiParam images Article's images.
 * @apiParam tags Article's tags.
 * @apiParam author Article's author.
 * @apiParam phone Article's phone.
 * @apiParam location Article's location.
 * @apiParam comments Article's comments.
 * @apiSuccess {Object} article Article's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Article not found.
 */
router.put('/:id',
	body({topic, description, images, tags, author, phone, location}),
	update);

/**
 * @api {delete} /articles/:id Delete article
 * @apiName DeleteArticle
 * @apiGroup Article
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Article not found.
 */
router.delete('/:id',
	destroy);

// add comment to article
router.post('/:id/comments',
	createComment);

export default router
