import {Article} from './'

let article;

beforeEach(async() => {
	article = await Article.create({
		topic: 'test',
		description: 'test',
		images: 'test',
		tags: 'test',
		author: 'test',
		phone: 'test',
		location: 'test',
		comments: 'test'
	})
});

describe('view', () => {
	it('returns simple view', () => {
		const view = article.view();
		expect(typeof view).toBe('object');
		expect(view.id).toBe(article.id);
		expect(view.topic).toBe(article.topic);
		expect(view.description).toBe(article.description);
		expect(view.images).toBe(article.images);
		expect(view.tags).toBe(article.tags);
		expect(view.author).toBe(article.author);
		expect(view.phone).toBe(article.phone);
		expect(view.location).toBe(article.location);
		expect(view.comments).toBe(article.comments);
		expect(view.createdAt).toBeTruthy();
		expect(view.updatedAt).toBeTruthy()
	});

	it('returns full view', () => {
		const view = article.view(true);
		expect(typeof view).toBe('object');
		expect(view.id).toBe(article.id);
		expect(view.topic).toBe(article.topic);
		expect(view.description).toBe(article.description);
		expect(view.images).toBe(article.images);
		expect(view.tags).toBe(article.tags);
		expect(view.author).toBe(article.author);
		expect(view.phone).toBe(article.phone);
		expect(view.location).toBe(article.location);
		expect(view.comments).toBe(article.comments);
		expect(view.createdAt).toBeTruthy();
		expect(view.updatedAt).toBeTruthy()
	})
});
