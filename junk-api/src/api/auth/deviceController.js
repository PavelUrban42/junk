import {success, notFound} from "../../services/response/";
import User from "../user/model";

export const find = ({params}, res, next) =>
	User.findByDevice(params.deviceId)
		.then(notFound(res))
		.then((user) => getUserProfile(user, params.deviceId))
		.then(success(res))
		.catch(next);


export const loginByDevice = (req, res, next) => {
	const hash = User.getHash(req.body.name);
	var email = `${req.body.name.replace(/\W+/g, '_').toLowerCase()}${hash}@junkapp.com`;
	var picture = `https://gravatar.com/avatar/${hash}?d=identicon`;
	var user = {
		service: `device`,
		id: req.body.deviceId,
		role: 'anonymous',
		name: req.body.name,
		email: email,
		picture: picture
	};
	User.createFromService(user)
		.then((user) => getUserProfile(user, req.body.deviceId))
		.then(success(res, 201))
		.catch(next);
};

const getUserProfile = function (user, deviceId) {
	if (user) {
		const token = User.getHash(deviceId);
		return {
			user: user.view(),
			token: token
		}
	} else {
		return null;
	}
};
