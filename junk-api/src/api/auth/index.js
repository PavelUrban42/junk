import {Router} from 'express'
import {login} from './googleController'
import {google} from '../../services/passport'
import {find, loginByDevice} from './deviceController';

const router = new Router();

/**
 * @api {post} /auth/google Authenticate with Google
 * @apiName AuthenticateGoogle
 * @apiGroup Auth
 * @apiParam {String} access_token Google user accessToken.
 * @apiSuccess (Success 201) {String} token User `access_token` to be passed to other requests.
 * @apiSuccess (Success 201) {Object} user Current user's data.
 * @apiError 401 Invalid credentials.
 */
router.post('/google',
	google(),
	login);

router.get('/device/:deviceId',
	find);

router.post('/device',
	loginByDevice);

export default router
