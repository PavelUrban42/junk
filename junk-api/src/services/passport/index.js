import passport from 'passport'
import {Strategy as BearerStrategy} from 'passport-http-bearer'
import {getUser} from '../../services/google/index'
import {Strategy as JwtStrategy, ExtractJwt} from 'passport-jwt'
import {jwtSecret, masterKey, googleAuth} from '../../config'
import User from '../../api/user/model'


export const google = () =>
	passport.authenticate('google', {session: false});

export const master = () =>
	passport.authenticate('master', {session: false});

export const token = ({required, roles = User.roles} = {}) => (req, res, next) =>
	passport.authenticate('token', {session: false}, (err, user, info) => {
		if (err || (required && !user) || (required && !~roles.indexOf(user.role))) {
			return res.status(401).end()
		}
		req.logIn(user, {session: false}, (err) => {
			if (err) return res.status(401).end();
			next()
		})
	})(req, res, next);

passport.use('google', new BearerStrategy((token, done) => {
	getUser(token).then((user) => {
		return User.createFromService(user)
	}).then((user) => {
		done(null, user);
		return null
	}).catch(done)
}));

passport.use('master', new BearerStrategy((token, done) => {
	if (token === masterKey) {
		done(null, {})
	} else {
		done(null, false)
	}
}));

passport.use('token', new JwtStrategy({
	secretOrKey: jwtSecret,
	jwtFromRequest: ExtractJwt.fromExtractors([
		ExtractJwt.fromUrlQueryParameter('access_token'),
		ExtractJwt.fromBodyField('access_token'),
		ExtractJwt.fromAuthHeaderWithScheme('Bearer')
	])
}, ({id}, done) => {
	User.findById(id).then((user) => {
		done(null, user);
		return null
	}).catch(done)
}));
