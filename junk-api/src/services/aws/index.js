import AWS from 'aws-sdk'
import uuidV4  from 'uuid/v4';
import Promise from 'promise';
import {awsConfig} from '../../config'

AWS.config.accessKeyId = awsConfig.accessKeyId;
AWS.config.secretAccessKey = awsConfig.secretAccessKey;
AWS.config.region = awsConfig.region;
var s3 = new AWS.S3({apiVersion: '2006-03-01'});

var uploadParams = {Bucket: awsConfig.bucket, Key: '', Body: '', ACL: 'public-read'};

export const uploadImage = function (image) {
	return new Promise(function (resolve, reject) {
		uploadParams.Body = image;
		uploadParams.Key = awsConfig.folder + '/' + uuidV4() + '.jpg';

		s3.upload(uploadParams, function (err, data) {
			if (err || !data) {
				console.log('Error', err);
				reject(err);
			} else {
				console.log('Upload Success', data.Location);
				resolve({url: data.Location});
			}
		});
	});
};
